package com.distance.calculator.businessUnitTest;

import com.distance.calculator.business.CalculatorService;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.Postcodelatlng;
import com.distance.calculator.model.response.ResponseResultDTO;
import com.distance.calculator.repository.CalculatorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CalculateDistanceTest {


    @Mock
    private CalculatorRepository calculatorRepository;
    @InjectMocks
    private CalculatorService underTest;

    @Test
    public void testGetDistance() throws MyResourceNotFoundException {

        Postcodelatlng locus1 = new Postcodelatlng(1L, "AB10 1XG", 57.14,-2.114);
        Postcodelatlng locus2 = new Postcodelatlng(2L, "AB10 6RN", 57.13,-2.121);

        Mockito.when(calculatorRepository.findByPostcode("AB10 1XG")).thenReturn(locus1);
        Mockito.when(calculatorRepository.findByPostcode("AB10 6RN")).thenReturn(locus2);

        ResponseResultDTO actual = underTest.calculateDistance(locus1.getpostcode(),locus2.getpostcode());
        ResponseResultDTO expected = new ResponseResultDTO(locus1.toString(),locus2.toString(),1.19);

        assertEquals(actual.getDistance(), expected.getDistance());
        assertEquals(actual.toString(), expected.toString());

    }

}