package com.distance.calculator.businessUnitTest;

import com.distance.calculator.business.CalculatorService;
import com.distance.calculator.exception.DataMissingException;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.Postcodelatlng;
import com.distance.calculator.model.response.ResponsePutResultDTO;
import com.distance.calculator.repository.CalculatorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(MockitoJUnitRunner.class)
public class UpdateLocationTest {
    @Mock
    private CalculatorRepository calculatorRepository;
    @InjectMocks
    private CalculatorService underTest;

    @Test
    public void testSave() throws MyResourceNotFoundException, DataMissingException {
        Postcodelatlng locus1 = new Postcodelatlng(1L,"AB10 1XG" , 57.14,-2.114);
        ResponsePutResultDTO expected = new ResponsePutResultDTO(57.14,-2.114);
        String postcode = "AB10 1XG";
        Mockito.when(calculatorRepository.findByPostcode("AB10 1XG")).thenReturn(locus1);

        ResponsePutResultDTO actual = underTest.updateCoordinates(expected,postcode);


        //assertEquals(actual, expected);
        assertEquals(actual.getLongitude(), expected.getLongitude(),0.001);

    }
}
