package com.distance.calculator.controllerUnitTest;


import com.distance.calculator.business.CalculatorService;
import com.distance.calculator.controller.CalculatorController;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.Postcodelatlng;
import com.distance.calculator.model.response.ResponseResultDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@GetMapping("/{id1},{id2}")
//public ResponseEntity getCalculatorResponse(@PathVariable Long id1, @PathVariable Long id2) throws MyResourceNotFoundException {
//        return ResponseEntity.ok(calculatorService.calculateDistance(id1, id2));
//        }
@RunWith(SpringRunner.class)
@WebMvcTest(CalculatorController.class)
@ContextConfiguration(classes = Postcodelatlng.class)
@WebAppConfiguration
public class ReturnResultTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;


    @MockBean
    private CalculatorService service;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }
    @Test
    @WithMockUser(value = "john")
    public void givenLocationsReturnJSONObjectResultTest()
            throws MyResourceNotFoundException, Exception {

        Postcodelatlng locus1 = new Postcodelatlng(1L, "AB10 1XG", 57.14, -2.114);
        Postcodelatlng locus2 = new Postcodelatlng(2L, "AB10 6RN", 57.13, -2.121);

        ResponseResultDTO expected = new ResponseResultDTO(locus1.toString(), locus2.toString(), 1.19);

        given(service.calculateDistance("AB10 1XG", "AB10 6RN")).willReturn(expected);

        mvc.perform(get("/public/1,2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
        //.andExpect(content().string(contains("1.19 km")));
    }
}
