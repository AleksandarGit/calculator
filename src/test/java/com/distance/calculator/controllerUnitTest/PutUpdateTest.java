package com.distance.calculator.controllerUnitTest;


import com.distance.calculator.business.CalculatorService;
import com.distance.calculator.controller.CalculatorController;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.Postcodelatlng;

import com.distance.calculator.model.response.ResponsePutResultDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CalculatorController.class)
@ContextConfiguration(classes = Postcodelatlng.class)
public class PutUpdateTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CalculatorService service;


    @Test
    @WithMockUser(username = "john")
    public void givenLocationsReturnJSONObjectLocation()
            throws MyResourceNotFoundException,Exception{
        ResponsePutResultDTO given = new ResponsePutResultDTO(57.14,-2.114);
        String postcode = "AB10 1XG";
        Postcodelatlng pass = service.getPostcodelatlngFromDB(postcode);

        when(service.updateCoordinates(given,"AB10 1XG")).thenReturn(given);

        RequestBuilder request = MockMvcRequestBuilders
                .put("/updateLocation/?param1=AB10 1XG")
               // .content(ResponsePutResultDTO given)
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mvc.perform(request)
                .andExpect(status().is4xxClientError())
                //.andExpect(content().json("{'id':1L,'postcode':'AB10 1XG','latitude':'57','longitude':'2'}"))
                .andReturn();

    }
}
