<!doctype html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, User!</title>
  </head>
  <body>
    <nav class="navbar navbar-inverse">
    		<div class="container">
    			<div class="navbar-header">
    				<a class="navbar-brand" href="#">Spring Boot</a>
    			</div>
    			<div id="navbar" class="collapse navbar-collapse">
    				<ul class="nav navbar-nav">
    					<li class="active"><a href="#">Home</a></li>
    					<li><a href="#about">About</a></li>
    				</ul>
    			</div>
    		</div>
    	</nav>

    	<div class="container">

    		<div class="starter-template">
    			<h1>Distance Calculator Example</h1>
    			<h2>Welcome Message: ${message}</h2>
    			<br></br>
    		</div>
    	</div>
        <div class="container">
            <div class="alert alert-success" role="alert">
                    <a href="/calculateDistance" class="alert-link">Click on this to calculate distance between two postal codes!</a>
            </div>
            <div class="alert alert-success" role="alert">
                     <a href="/update" class="alert-link">Click on this to update post codes!</a>
            </div>
         </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>