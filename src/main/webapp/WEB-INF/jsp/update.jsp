<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<body>



	<div class="container">

		<div class="starter-template">
			<h1>Distance Calculator Example</h1>
			<h3>Insert postal code latitude and longitude</h3>
<form:form method="POST" action="/updatePostcode">
   <table>

    <tr>
        <td><form:label path="postcode">Postcode</form:label></td>
        <td><form:input name="postcode" path="postcode" /></td>
        <form:errors path="postcode"  cssClass="error"/>
    </tr>
    <tr>
        <td><form:label path="latitude">Latitude</form:label></td>
        <td><form:input name="latitude" path="latitude" /></td>
        <form:errors path="latitude"  cssClass="error"/>
    </tr>
    <tr>
        <td><form:label path="longitude">Longitude</form:label></td>
        <td><form:input name="longitude" path="longitude" /></td>
        <form:errors path="longitude"  cssClass="error"/>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
    </table>
</form:form>
Click on this <strong><a href="/">link</a></strong> to visit previous page.
		</div>

	</div>

 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>

</html>