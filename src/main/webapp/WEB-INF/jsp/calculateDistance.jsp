<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>

<h2>Insert two postal code to calculate distance</h2>
<div class="jumbotron">
    <form:form method="POST" action="/result">
        <table>
            <tr>
                <td><form:label path="postcode1">Postal code 1:</form:label></td>
                <td><form:input name="postcode1" path="postcode1" /></td>
            </tr>
            <tr>
                <td><form:label path="postcode2">Postal code 2:</form:label></td>
                <td><form:input name="postcode2" path="postcode2" /></td>
            </tr>
            <tr>
                <td colspan="2">
                <input type="submit" value="Submit"/>
                </td>
            </tr>
        </table>
</form:form>
<p>To calculate the distance between two postal codes insert them into the form above, then press submit!</p>
  <a class="btn btn-primary btn-lg" href="/" role="button"><strong>Back</strong></a>
</div>

 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>