package com.distance.calculator.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Postcodelatlng {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotNull
    private String postcode;
    @NotNull
    private double latitude;
    @NotNull
    private double longitude;

    public Postcodelatlng() {
    }

    public Postcodelatlng(long id, @NotNull String postcode, @NotNull double latitude, @NotNull double longitude) {
        this.id = id;
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Postcodelatlng(@NotNull String postcode, @NotNull double latitude, @NotNull double longitude) {
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getpostcode() {
        return postcode;
    }

    public void setpostcode(String postcode) {
        this.postcode = postcode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    @Override
    public String toString() {
        return new StringBuilder()
                .append("Postal Code :" + postcode + "      ")
                .append("Geographical latitude :" + latitude + "      ")
                .append("Geographical longitude :" + longitude)
                .toString();
    }

}
