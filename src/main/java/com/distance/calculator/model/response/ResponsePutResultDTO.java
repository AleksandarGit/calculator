package com.distance.calculator.model.response;

import com.distance.calculator.model.Postcodelatlng;

public class ResponsePutResultDTO {
    private double latitude;
    private double longitude;

    public ResponsePutResultDTO() {
    }

    public ResponsePutResultDTO(Postcodelatlng dataUnit) {
        this.latitude = dataUnit.getLatitude();
        this.longitude = dataUnit.getLongitude();
    }

    public ResponsePutResultDTO(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Changed postal code has new coordinates: {" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
