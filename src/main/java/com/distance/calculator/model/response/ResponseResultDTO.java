package com.distance.calculator.model.response;

public class ResponseResultDTO {
    private static final String unit = "km";

    private String postcode1;
    private String postcode2;
    private Double distance;

    public ResponseResultDTO(String postcode1, String postcode2, Double distance) {

        this.postcode1 = postcode1;
        this.postcode2 = postcode2;
        this.distance = distance;
    }

    public ResponseResultDTO (){}

    public String getUnit() {
        return unit;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public String getPostcode2() {
        return postcode2;
    }

    public void setPostcode2(String postcode2) {
        this.postcode2 = postcode2;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Result is :{" +
                " postcode1='" + postcode1 + '\'' +
                ", postcode2='" + postcode2 + '\'' +
                ", distance=" + String.format("%.2f", distance) +
                " " + unit + '\'' +
                '}';
    }


}
