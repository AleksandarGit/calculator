package com.distance.calculator.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateRequest {


    @NotNull(message = "is required")
    @Size(min = 8, max = 8, message = "valid postcode must have 8 characters")
    private String postcode;
    @NotNull
    private double latitude;
    @NotNull
    private double longitude;

    public UpdateRequest(String postcode, double latitude, double longitude) {
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public UpdateRequest() {

    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
