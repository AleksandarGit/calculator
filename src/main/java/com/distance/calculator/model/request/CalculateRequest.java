package com.distance.calculator.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CalculateRequest {
    @NotNull(message="is required")
    @Size(min = 8, max = 8, message = "valid postcode must have 8 characters")
    private String postcode1;

    @NotNull(message="is required")
    @Size(min = 8, max = 8, message = "valid postcode must have 8 characters")
    private String postcode2;



    public CalculateRequest() {
           }

    public CalculateRequest(String postcode1, String postcode2) {
        this.postcode1 = postcode1;
        this.postcode2 = postcode2;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public String getPostcode2() {
        return postcode2;
    }

    public void setPostcode2(String postcode2) {
        this.postcode2 = postcode2;
    }

    @Override
    public String toString() {
        return "CalculateRequest{" +
                "postcode1='" + postcode1 + '\'' +
                ", postcode2='" + postcode2 + '\'' +
                '}';
    }
}
