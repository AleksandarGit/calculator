package com.distance.calculator.business;

import com.distance.calculator.exception.DataMissingException;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.request.UpdateRequest;
import com.distance.calculator.model.response.ResponsePutResultDTO;
import com.distance.calculator.model.response.ResponseResultDTO;


public interface CalculatorServiceInterface {


    ResponseResultDTO calculateDistance(String id1, String id) throws MyResourceNotFoundException;

    ResponsePutResultDTO updateCoordinates(ResponsePutResultDTO location, String postcode) throws MyResourceNotFoundException, DataMissingException;

}