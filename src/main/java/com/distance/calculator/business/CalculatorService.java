package com.distance.calculator.business;

import com.distance.calculator.Util.UtilDistanceCalculaton;
import com.distance.calculator.exception.DataMissingException;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.Postcodelatlng;
import com.distance.calculator.model.request.UpdateRequest;
import com.distance.calculator.model.response.ResponsePutResultDTO;
import com.distance.calculator.model.response.ResponseResultDTO;
import com.distance.calculator.repository.CalculatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;


@Service
public class CalculatorService implements CalculatorServiceInterface {

    @Autowired
    private CalculatorRepository calculatorRepository;
    DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );

    @Override
    public ResponseResultDTO calculateDistance(String id1, String id2) throws MyResourceNotFoundException {

        Postcodelatlng opLocal1 = calculatorRepository.findByPostcode(id1);
        Postcodelatlng opLocal2 = calculatorRepository.findByPostcode(id2);

        String opLocal1Str = opLocal1.toString();
        String opLocal2Str = opLocal2.toString();
        double calculation = UtilDistanceCalculaton.calculateDistanceFromLatLng(opLocal1.getLatitude(), opLocal1.getLongitude(), opLocal2.getLatitude(), opLocal2.getLongitude());
        double calculationFormated = new Double(df2.format(calculation)).doubleValue();
        ResponseResultDTO result1 = new ResponseResultDTO(opLocal1Str, opLocal2Str, calculationFormated);


        return result1;
    }

    public Postcodelatlng getPostcodelatlngFromDB(String postcode) {
        Postcodelatlng opLocal1 = calculatorRepository.findByPostcode(postcode);
        return opLocal1;
    }

    @Override
    public ResponsePutResultDTO updateCoordinates(ResponsePutResultDTO location, String postcode) throws MyResourceNotFoundException, DataMissingException {



        Postcodelatlng current = calculatorRepository.findByPostcode(postcode);

        current.setLatitude(location.getLatitude());
        current.setLongitude(location.getLongitude());

        ResponsePutResultDTO responsePutResultDTO = new ResponsePutResultDTO(current);
        return responsePutResultDTO;
    }


}
