package com.distance.calculator.controller;

import com.distance.calculator.business.CalculatorService;
import com.distance.calculator.exception.DataMissingException;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.Postcodelatlng;
import com.distance.calculator.model.response.ResponsePutResultDTO;
import com.distance.calculator.model.response.ResponseResultDTO;
import com.distance.calculator.repository.CalculatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;


//@RestController
@Transactional
@EnableJpaRepositories(basePackageClasses = CalculatorRepository.class)
public class CalculatorController {


    @Autowired
    private CalculatorService calculatorService;


    @GetMapping("/")
    public ResponseEntity<ResponseResultDTO> getCalculatorResponse(@PathParam("param1") String param1, @PathParam("param2") String param2) throws MyResourceNotFoundException {
        return ResponseEntity.ok(calculatorService.calculateDistance(param1, param2));
    }

    @GetMapping("/getOne/")
    public ResponseEntity<Postcodelatlng> getPostcodelatlngResponse(@PathParam("param1") String param1) throws MyResourceNotFoundException {

        return ResponseEntity.ok(calculatorService.getPostcodelatlngFromDB(param1));
    }

    @PutMapping("/updateLocation/")
    public ResponseEntity updateBase(@PathParam("param1") String param1, @RequestBody @Valid ResponsePutResultDTO location) throws MyResourceNotFoundException, DataMissingException {
        return ResponseEntity.ok(calculatorService.updateCoordinates(location, param1));
    }

}
