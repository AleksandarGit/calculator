package com.distance.calculator.controller;

import com.distance.calculator.business.CalculatorService;
import com.distance.calculator.exception.DataMissingException;
import com.distance.calculator.exception.MyResourceNotFoundException;
import com.distance.calculator.model.request.CalculateRequest;
import com.distance.calculator.model.request.UpdateRequest;
import com.distance.calculator.model.response.ResponsePutResultDTO;
import com.distance.calculator.model.response.ResponseResultDTO;
import com.distance.calculator.repository.CalculatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;

import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@Transactional
@EnableJpaRepositories(basePackageClasses = CalculatorRepository.class)
public class CalculatorJSPController {


    @Autowired
    private CalculatorService calculatorService;
    private ResponsePutResultDTO location;
    private CalculateRequest calculateRequest;
    private UpdateRequest updateRequest;

    private String message = "Hello User 007";

    private ResponseResultDTO resultFromLoc1Loc2;


    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        model.put("message", this.message);
        return "welcome";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView responsePutResultDTO() {
        return new ModelAndView("update", "command", new UpdateRequest());
    }

    @RequestMapping(value = "/updatePostcode", method = RequestMethod.POST)
    public String hello(@Valid @ModelAttribute("updateRequest") UpdateRequest updateRequest, ModelMap model) throws DataMissingException, MyResourceNotFoundException {
        if ((updateRequest.getPostcode() == null) || (updateRequest.getLatitude() == 0)|| (updateRequest.getLongitude() == 0)) {
            throw new DataMissingException("Invalid data for postalcode or latitude or longitude!");
        }
        ResponsePutResultDTO reqw = new ResponsePutResultDTO(updateRequest.getLatitude(), updateRequest.getLongitude());
        if ((reqw.getLongitude() == 0)|| (reqw.getLatitude() == 0)) {
            throw new MyResourceNotFoundException("No such postcode in the database.");
        }

        ResponsePutResultDTO resp = calculatorService.updateCoordinates (reqw, updateRequest.getPostcode());

                   model.addAttribute("updateResult",resp);

            return "updatePostcode";

    }

        @RequestMapping(value = "/calculateDistance", method = RequestMethod.GET)
    public ModelAndView calculateRequest() {
        return new ModelAndView("calculateDistance", "command", new CalculateRequest());
    }


    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String result(@Valid @ModelAttribute("calculateRequest") CalculateRequest calculateRequest, ModelMap model) throws MyResourceNotFoundException, DataMissingException {

        if ((calculateRequest.getPostcode1() == null) || (calculateRequest.getPostcode2() == null)) {
            throw new DataMissingException("Invalid data for postalcodes!");
        }

        ResponseResultDTO product = calculatorService.calculateDistance
                (calculateRequest.getPostcode1(), calculateRequest.getPostcode2());
        if (product.getDistance() == 0) {
            throw new MyResourceNotFoundException("You entered the same postcode twice!");
        }

        if ((product.getPostcode1() == null)|| (product.getPostcode2() == null)) {
            throw new MyResourceNotFoundException("No such postcode in the database.");
        }

        model.addAttribute("result",product);

                   return "result";

    }

}
