package com.distance.calculator.repository;

import com.distance.calculator.model.Postcodelatlng;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculatorRepository extends CrudRepository<Postcodelatlng, Long> {
    //@Query("select p from Postcodelatlng where p.postcode = :postcode")
    public Postcodelatlng findByPostcode(String postcode);

//    @Query("select u from User u where u.firstname = :firstname or u.lastname = :lastname")
//    User findByLastnameOrFirstname(@Param("lastname") String lastname,
//                                   @Param("firstname") String firstname);
}
