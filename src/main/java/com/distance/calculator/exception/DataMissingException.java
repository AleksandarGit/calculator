package com.distance.calculator.exception;

public class DataMissingException extends Exception {

    public DataMissingException(final String message) {
        super(message);
    }
}
