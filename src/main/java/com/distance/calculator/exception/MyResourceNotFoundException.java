package com.distance.calculator.exception;

public class MyResourceNotFoundException extends Throwable {

    public MyResourceNotFoundException(final String message) {
        super(message);
    }
}

